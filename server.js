//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var requestjson = require('request-json');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/dbbanca4mb05061/collections/movimientos?apiKey=D61-PSRYyiYq21Ygvm6Rworm1SzAd-DW";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
//  res.send('Hola Mundo'); //RES GET enviando texto
  res.sendfile(path.join(__dirname,"index.html"));
});
app.get("/clientes/:idcliente", function(req, res){
//  res.send('Hola Mundo');
  res.send("Aqui tiene al cliente numero :" + req.params.idcliente);
});
app.post("/", function(req, res){
  res.send("Hemos recibido su peticiòn de POST cambiada")
});
app.put("/", function(req, res){
  res.send("Hemos recibido su peticiòn de PUT")
});
app.delete("/", function(req, res){
  res.send("Hemos recibido su peticiòn de DELETE")
});

app.get("/movimientos", function(req, res){
  clienteMLab.get('', function(err, resM, body){
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  });
});

app.post("/movimientos", function(req, res){
  clienteMLab.post("", req.body,function(err, resM, body){
    res.send(body);
  })
});
